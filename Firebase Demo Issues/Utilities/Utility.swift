//
//  Utility.swift
//  Firebase Demo Issues
//
//  Created by Prashant Pandey  on 30/09/20.
//  Copyright © 2020 Prashant Pandey. All rights reserved.
//

import Foundation
import UIKit

class Utility {
     public static func createAttributedText(fullStr: String, toColor: String, colour: UIColor) -> NSMutableAttributedString {
        let main_string = fullStr
        let string_to_color = toColor
        let range = (main_string as NSString).range(of: string_to_color)
        let attribute = NSMutableAttributedString.init(string: main_string)
        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: colour , range: range)
        return attribute
    }
    
    public static func showAlert(title:String?, messageString:String, buttons:[String]?, viewController:UIViewController, didSelect:((_ buttonTitle:String) -> Void)?) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: messageString, preferredStyle: .alert)
            alertController.view.tintColor = UIColor(red:0.22, green:0.28, blue:0.31, alpha:1.0)
            
            if buttons != nil, buttons!.count > 0 {
                for button:String in buttons! {
                    let defaultAction = UIAlertAction(title: button, style: .default, handler: { (alertAction) in
                        if didSelect != nil {
                            didSelect!(button)
                        }
                    })
                    alertController.addAction(defaultAction)
                }
            } else {
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: { (alertAction) in
                    if didSelect != nil {
                        didSelect!("OK")
                    }
                })
                alertController.addAction(defaultAction)
            }
            viewController.present(alertController, animated: true, completion: nil)
        }
    }
}
