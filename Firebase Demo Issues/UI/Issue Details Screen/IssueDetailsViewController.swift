//
//  IssueDetailsViewController.swift
//  Firebase Demo Issues
//
//  Created by Prashant Pandey  on 30/09/20.
//  Copyright © 2020 Prashant Pandey. All rights reserved.
//

import Foundation
import UIKit

class IssueDetailsViewController: UIViewController {
    
    var number: Int?
    @IBOutlet weak var tableForIssueComment: UITableView!
    private let client = ApiClient()
    var progressIndicator: LoadingIndicator!
    
    private let viewModel = IssueDetailViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchIssueCommentsFromApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = constants.vcTitle.TITLE_ISSUE_DETAIL
    }
    
    func fetchIssueCommentsFromApi() {
        if Reachability.isConnectedToNetwork() {            
            showProgressIndicator()

            client.getIssueCommentDetail(from: .issueDetail(self.number)) { result in
                weak var weakSelf = self
                self.progressIndicator.stopIndicator()
            
                switch result {
                case .success(let result):
                    if let issues = result, issues.count > 0 {
                        weakSelf?.viewModel.getCommentsList(issues: issues)
                        weakSelf?.tableForIssueComment.reloadData()
                    } else {
                         Utility.showAlert(title: nil, messageString: constants.ERROR_MESSAGE, buttons: nil, viewController: self) { _ in
                             self.navigationController?.popViewController(animated: true)
                         }
                    }
                case .failure(let error):
                    Utility.showAlert(title: nil, messageString: "the error \(error)", buttons: nil, viewController: self, didSelect: nil)
                    print("the error \(error)")
                }
            }
        } else {
           self.progressIndicator.stopIndicator()
            Utility.showAlert(title: nil, messageString: constants.ERROR_MESSAGE, buttons: nil, viewController: self) { _ in
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    private func showProgressIndicator() {
        if progressIndicator == nil {
            progressIndicator = LoadingIndicator(frame: view.frame)
            progressIndicator.startIndicator(message: "Loading...", on: view)
        } else if progressIndicator.alertIndicator.isAnimating == false {
            progressIndicator = LoadingIndicator(frame: view.frame)
            progressIndicator.startIndicator(message: "Loading...", on: view)
        }
    }
}

extension IssueDetailsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CommentTableViewCell = tableView.dequeueReusableCell(withIdentifier: CommentTableViewCell.identifier, for: indexPath) as! CommentTableViewCell
        cell.selectionStyle = .none
        if self.viewModel.count > 0 {
            let cellViewModel = viewModel.cellViewModel(index: indexPath.row)
            cell.viewModel = cellViewModel
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
