//
//  IssueDetailViewModel.swift
//  Firebase Demo Issues
//
//  Created by Prashant Pandey  on 30/09/20.
//  Copyright © 2020 Prashant Pandey. All rights reserved.
//

import Foundation

class IssueDetailViewModel {
    
    private var issues:[Issue] = [Issue]()
    
    public func getCommentsList(issues: [Issue]) {
        self.issues = issues
    }
    
    public func cellViewModel(index: Int) -> IssueTableCellDetailModel? {
        let issueTableCellDetailModel = IssueTableCellDetailModel(issue: issues[index])
        return issueTableCellDetailModel
    }
    
    public var count: Int {
        return issues.count
    }
}
