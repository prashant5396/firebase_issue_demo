//
//  IssueTableCellDetailModel.swift
//  Firebase Demo Issues
//
//  Created by Prashant Pandey  on 30/09/20.
//  Copyright © 2020 Prashant Pandey. All rights reserved.
//

import Foundation

class IssueTableCellDetailModel {
    
    private let issue: Issue
    
    init(issue: Issue) {
        self.issue = issue
    }
    
    var body: String {
        if let body = issue.body {
            return body
        }
        return constants.EMPTY_STRING
    }
}
