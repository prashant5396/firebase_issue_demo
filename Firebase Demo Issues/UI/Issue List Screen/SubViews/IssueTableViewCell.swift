//
//  IssueTableViewCell.swift
//  Firebase Demo Issues
//
//  Created by Prashant Pandey  on 30/09/20.
//  Copyright © 2020 Prashant Pandey. All rights reserved.
//

import Foundation
import UIKit

class IssueTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblForTitle: UILabel!
    @IBOutlet weak var lblForDescription: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public var viewModel: IssueTableCellModel? {
        didSet {
            guard let viewModel = viewModel else { return }
            self.lblForTitle.text = viewModel.title
            self.lblForDescription.attributedText = viewModel.body
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}
