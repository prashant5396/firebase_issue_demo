//
//  Issue.swift
//  Firebase Demo Issues
//
//  Created by Prashant Pandey  on 30/09/20.
//  Copyright © 2020 Prashant Pandey. All rights reserved.
//

import Foundation

struct Issue: Decodable {
    let title: String?
    let number: Int?
    let body: String?
}
