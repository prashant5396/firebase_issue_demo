//
//  IssueListViewController.swift
//  Firebase Demo Issues
//
//  Created by Prashant Pandey  on 30/09/20.
//  Copyright © 2020 Prashant Pandey. All rights reserved.
//

import Foundation
import UIKit

class IssueListViewController: UIViewController {
    
    @IBOutlet weak var tableForIssueList: UITableView!
    private let viewModel = IssueListViewModel()
    private let client = ApiClient()
    var progressIndicator: LoadingIndicator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchIssueListFromApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = constants.vcTitle.TITLE_ISSUE_LIST
    }
    
    func fetchIssueListFromApi() {
        if Reachability.isConnectedToNetwork() {
            showProgressIndicator()
          
            client.getIssueListFeed(from: .issueList) { result in
                weak var weakSelf = self
                self.progressIndicator.stopIndicator()

                switch result {
                case .success(let result):
                    if let list = result, list.count > 0 {
                        weakSelf?.viewModel.getIssuesList(issued: list)
                        weakSelf?.tableForIssueList.reloadData()
                    } else {
                        Utility.showAlert(title: nil, messageString: constants.ERROR_MESSAGE, buttons: nil, viewController: self, didSelect: nil)
                    }
                case .failure(let error):
                    Utility.showAlert(title: nil, messageString: "the error \(error)", buttons: nil, viewController: self, didSelect: nil)
                    print("the error \(error)")
                }
            }
        } else {
            self.progressIndicator.stopIndicator()
            Utility.showAlert(title: nil, messageString: constants.ERROR_MESSAGE, buttons: nil, viewController: self, didSelect: nil)
        }
    }
    
    private func showProgressIndicator() {
        if progressIndicator == nil {
            progressIndicator = LoadingIndicator(frame: view.frame)
            progressIndicator.startIndicator(message: "Loading...", on: view)
        } else if progressIndicator.alertIndicator.isAnimating == false {
            progressIndicator = LoadingIndicator(frame: view.frame)
            progressIndicator.startIndicator(message: "Loading...", on: view)
        }
    }
}

extension IssueListViewController :  UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : IssueTableViewCell = tableView.dequeueReusableCell(withIdentifier: IssueTableViewCell.identifier, for: indexPath) as! IssueTableViewCell
        cell.selectionStyle = .none
        if self.viewModel.count > 0 {
            let cellViewModel = viewModel.cellViewModel(index: indexPath.row)
            cell.viewModel = cellViewModel
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension IssueListViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let detailVC = self.storyboard!.instantiateViewController(withIdentifier: constants.vc.VC_ISSUE_DETAIL) as! IssueDetailsViewController
        let cellViewModel = viewModel.cellViewModel(index: indexPath.row)
        detailVC.number = cellViewModel!.number
        self.navigationController!.pushViewController(detailVC, animated: true)
    }
}
