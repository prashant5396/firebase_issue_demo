//
//  IssueTableCellModel.swift
//  Firebase Demo Issues
//
//  Created by Prashant Pandey  on 30/09/20.
//  Copyright © 2020 Prashant Pandey. All rights reserved.
//

import Foundation
import UIKit

class IssueTableCellModel {
    
    private let issue: Issue
    
    init(issue: Issue) {
        self.issue = issue
    }

    var title: String {
        return issue.title ?? constants.EMPTY_STRING
    }
    var commentDetail: IssueTableCellDetailModel?
    
    var body: NSMutableAttributedString {
        if let body = issue.body {
            if body.count > 140 {
                return Utility.createAttributedText(fullStr: String(format: "%@...SeeFull", body[0..<140]), toColor: "SeeFull", colour: UIColor.blue)
            } else {
                return NSMutableAttributedString.init(string: body)
            }
        }
        return NSMutableAttributedString.init(string: constants.EMPTY_STRING)
    }
    
    var number: Int {
        return issue.number ?? 0
    }
}
