//
//  IssueListViewModel.swift
//  Firebase Demo Issues
//
//  Created by Prashant Pandey  on 30/09/20.
//  Copyright © 2020 Prashant Pandey. All rights reserved.
//

import Foundation

class IssueListViewModel {
    
    private var issues:[Issue] = [Issue]()
    
    public func getIssuesList(issued: [Issue]) {
        self.issues = issued
    }
    
    public func cellViewModel(index: Int) -> IssueTableCellModel? {
        let issueTableCellModel = IssueTableCellModel(issue: issues[index])
        return issueTableCellModel
    }
    
    public var count: Int {
        return issues.count
    }
    
    public func selectedUserLogin(index: Int) -> Int {
        return issues[index].number ?? 0
    }
}
