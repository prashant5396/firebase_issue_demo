//
//  Constants.swift
//  Firebase Demo Issues
//
//  Created by Prashant Pandey  on 30/09/20.
//  Copyright © 2020 Prashant Pandey. All rights reserved.
//

import Foundation

struct constants {
    static let EMPTY_STRING = ""
    static let APP_NAME = "GitHubIssuesDemo"
    static let ERROR_MESSAGE = "Comments not available"
    static let ERROR_NETWORK = "No Internet found. Check your connection or try again."
    
    struct vcTitle {
        static let TITLE_ISSUE_LIST = "Issue List"
        static let TITLE_ISSUE_DETAIL = "Comments Detail"
    }
    
    struct vc {
        static let VC_ISSUE_DETAIL = "IssueDetailVC"
    }
}
