//
//  LoadingIndicator.swift
//  Firebase Demo Issues
//
//  Created by Prashant Pandey  on 30/09/20.
//  Copyright © 2020 Prashant Pandey. All rights reserved.
//

import Foundation
import UIKit

class LoadingIndicator: UIView {
    
    var alertIndicator:UIActivityIndicatorView!
    var alertMessageLabel:UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.clear
        
        let alertView:UIView = UIView()
        alertView.backgroundColor = UIColor.white
        alertView.layer.cornerRadius = 10.0
        addSubview(alertView)
        alertView.translatesAutoresizingMaskIntoConstraints = false
        
        alertView.layer.cornerRadius = 10.0
        alertView.layer.shadowColor = UIColor.black.cgColor
        alertView.layer.shadowOpacity = 0.8
        alertView.layer.shadowOffset = CGSize(width: 1, height: 1)
        alertView.layer.shadowRadius = 10
        
        NSLayoutConstraint(item: alertView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: alertView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: alertView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 220).isActive = true
        NSLayoutConstraint(item: alertView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 88).isActive = true
        
        alertIndicator = UIActivityIndicatorView(style: .large)
        alertIndicator.color = UIColor(red:0.22, green:0.28, blue:0.31, alpha:1.0)
        alertView.addSubview(alertIndicator)
        alertIndicator.translatesAutoresizingMaskIntoConstraints = false
        
        guard let alertIndicator = alertIndicator else {
            return
        }
        NSLayoutConstraint(item: alertIndicator, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 67).isActive = true
        NSLayoutConstraint(item: alertIndicator, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 67).isActive = true
        NSLayoutConstraint(item: alertIndicator, attribute: .leadingMargin, relatedBy: .equal, toItem: alertView, attribute: .leadingMargin, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: alertIndicator, attribute: .centerY, relatedBy: .equal, toItem: alertView, attribute: .centerYWithinMargins, multiplier: 1.0, constant: 0).isActive = true
        
        alertMessageLabel = UILabel()
        alertMessageLabel.numberOfLines = 0
        alertMessageLabel.textAlignment = .center
        alertMessageLabel.textColor = UIColor(red:0.22, green:0.28, blue:0.31, alpha:1.0)
        alertView.addSubview(alertMessageLabel)
        alertMessageLabel.translatesAutoresizingMaskIntoConstraints = false
        
        guard let alertMessageLabel = alertMessageLabel else {
            return
        }
        NSLayoutConstraint(item: alertMessageLabel, attribute: .trailingMargin, relatedBy: .equal, toItem: alertView, attribute: .trailingMargin, multiplier: 1.0, constant: -8).isActive = true
        NSLayoutConstraint(item: alertMessageLabel, attribute: .leadingMargin, relatedBy: .equal, toItem: alertIndicator, attribute: .trailingMargin, multiplier: 1.0, constant: 8).isActive = true
        NSLayoutConstraint(item: alertMessageLabel, attribute: .bottomMargin, relatedBy: .equal, toItem: alertView, attribute: .bottomMargin, multiplier: 1.0, constant: -8).isActive = true
        NSLayoutConstraint(item: alertMessageLabel, attribute: .topMargin, relatedBy: .equal, toItem: alertView, attribute: .topMargin, multiplier: 1.0, constant: 8).isActive = true
        
    }
    
    func startIndicator(message:String, on view:UIView) {
        if alertIndicator.isAnimating == false {
            alertIndicator.startAnimating()
            alertMessageLabel.text = message
            view.addSubview(self)
        }
    }
    
    func stopIndicator() {
        alertIndicator.stopAnimating()
        removeFromSuperview()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
