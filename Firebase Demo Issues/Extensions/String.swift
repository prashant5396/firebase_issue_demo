//
//  String.swift
//  Firebase Demo Issues
//
//  Created by Prashant Pandey  on 30/09/20.
//  Copyright © 2020 Prashant Pandey. All rights reserved.
//

import Foundation

extension String {
    subscript(_ range: CountableRange<Int>) -> String {
        let idx1 = index(startIndex, offsetBy: max(0, range.lowerBound))
        let idx2 = index(startIndex, offsetBy: min(self.count, range.upperBound))
        return String(self[idx1..<idx2])
    }
}
